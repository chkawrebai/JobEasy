package Job.Module;


import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Post
 *
 */
@Entity

public class Post implements Serializable {

	   
	@Id
	private int idPost;
	private Date PostDate;
	@Column(length=700)
	private String PostMsg;
	@ManyToOne
	@JoinColumn(name="id_user")
    private User users;
	@OneToMany(mappedBy="posts")
	private List<media> Medias;
	@OneToMany(mappedBy="Posts")
	private List<Comment> Comments;
	
	public User getUsers() {
		return users;
	}
	public void setUsers(User users) {
		this.users = users;
	}
	public List<media> getMedias() {
		return Medias;
	}
	public void setMedias(List<media> medias) {
		Medias = medias;
	}
	public List<Comment> getComments() {
		return Comments;
	}
	public void setComments(List<Comment> comments) {
		Comments = comments;
	}
	
	private static final long serialVersionUID = 1L;

	public Post() {
		super();
	}   
	public int getIdPost() {
		return this.idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}   
	public Date getPostDate() {
		return this.PostDate;
	}

	public void setPostDate(Date PostDate) {
		this.PostDate = PostDate;
	}   
	public String getPostMsg() {
		return this.PostMsg;
	}

	public void setPostMsg(String PostMsg) {
		this.PostMsg = PostMsg;
	}
   
}
