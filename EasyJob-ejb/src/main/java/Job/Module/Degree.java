package Job.Module;

 
import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Degree
 *
 */
@Entity

public class Degree implements Serializable {

	   
	@Id
	private int IdDegree;
	private String NameDiploma;
	private Date date_obt;
	@OneToMany(mappedBy="dgr")
	private List<Offre> offres;
	public List<Offre> getOffres() {
		return offres;
	}
	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}

	private static final long serialVersionUID = 1L;

	public Degree() {
		super();
	}   
	public int getIdDegree() {
		return this.IdDegree;
	}

	public void setIdDegree(int IdDegree) {
		this.IdDegree = IdDegree;
	}   
	public String getNameDiploma() {
		return this.NameDiploma;
	}

	public void setNameDiploma(String NameDiploma) {
		this.NameDiploma = NameDiploma;
	}   
	public Date getDate_obt() {
		return this.date_obt;
	}

	public void setDate_obt(Date date_obt) {
		this.date_obt = date_obt;
	}
   
}
