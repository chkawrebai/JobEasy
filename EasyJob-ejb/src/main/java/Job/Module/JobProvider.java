package Job.Module;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobProvider
 *
 */
@Entity

public class JobProvider  implements Serializable {

	   
	@Id
	@GeneratedValue
	private int id_JobProvider;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL,mappedBy="JobProviders")
	private List<User> users;
	public int getId_JobProvider() {
		return id_JobProvider;
	}
	public void setId_JobProvider(int id_JobProvider) {
		this.id_JobProvider = id_JobProvider;
	}
 


	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}



	private String companyName;
	@OneToMany(mappedBy="jobprovider")
	private List<Offre> offres;
	public List<Offre> getOffres() {
		return offres;
	}
	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	private static final long serialVersionUID = 1L;

	public JobProvider() {
		super();
	}   

   
}
