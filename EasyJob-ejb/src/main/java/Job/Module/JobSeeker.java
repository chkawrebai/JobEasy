package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobSeeker
 *
 */
@Entity

public class JobSeeker   implements Serializable {

	
	@Id
	@GeneratedValue
	private int id_JobSeeker;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL,mappedBy="JobSeekers")
	private List<User> Users;
	public List<User> getUsers() {
		return Users;
	}





	public void setUsers(List<User> users) {
		Users = users;
	}





	public int getId_JobSeeker() {
		return id_JobSeeker;
	}





	public void setId_JobSeeker(int id_JobSeeker) {
		this.id_JobSeeker = id_JobSeeker;
	}




 

	private String FirstName;
	private String LastName;
	private Date DateNaissance;
	private String Sexe;
	

 

	private static final long serialVersionUID = 1L;

	public JobSeeker() {
		super();
	}   

 
 

   
	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}   
	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}   
	public Date getDateNaissance() {
		return this.DateNaissance;
	}

	public void setDateNaissance(Date DateNaissance) {
		this.DateNaissance = DateNaissance;
	}   
	public String getSexe() {
		return this.Sexe;
	}

	public void setSexe(String Sexe) {
		this.Sexe = Sexe;
	}
   
}
