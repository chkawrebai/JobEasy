package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CV
 *
 */
@Entity

public class CV implements Serializable {

	@Id
	private int idCV;
	public JobSeeker getId_JobSeeker() {
		return id_JobSeeker;
	}
	public void setId_JobSeeker(JobSeeker id_JobSeeker) {
		this.id_JobSeeker = id_JobSeeker;
	}

	private String firstName;
	private String lastName;
	private Date birthday;
	@OneToMany(mappedBy="cvs")
	private List<Experience> Experiences;
	@ManyToOne
	@JoinColumn(name="id_JobSeeker")
    private JobSeeker id_JobSeeker;
  
	public List<Experience> getExperiences() {
		return Experiences;
	}
	public void setExperiences(List<Experience> experiences) {
		Experiences = experiences;
	}
	public JobSeeker getJobSeekers() {
		return id_JobSeeker;
	}
	public void setJobSeekers(JobSeeker jobSeekers) {
		id_JobSeeker = jobSeekers;
	}

	private static final long serialVersionUID = 1L;

	public CV() {
		super();
	}   
	public int getIdCV() {
		return this.idCV;
	}

	public void setIdCV(int idCV) {
		this.idCV = idCV;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
   
}
