package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comment
 *
 */
@Entity

public class Comment implements Serializable {

	   
	@Id
	private int CommentId;
	@Column(length=700)
	private String CommentText;
	private Date CommentDate;
	@ManyToOne
	@JoinColumn(name="id_user")
    private User users;
	@ManyToOne
	@JoinColumn(name="id_post")
    private Post Posts;
	
	public User getUsers() {
		return users;
	}
	public void setUsers(User users) {
		this.users = users;
	}

	public Post getPosts() {
		return Posts;
	}
	public void setPosts(Post posts) {
		this.Posts = posts;
	}
	private static final long serialVersionUID = 1L;

	public Comment() {
		super();
	}   
	public int getCommentId() {
		return this.CommentId;
	}

	public void setCommentId(int CommentId) {
		this.CommentId = CommentId;
	}   
	public String getCommentText() {
		return this.CommentText;
	}

	public void setCommentText(String CommentText) {
		this.CommentText = CommentText;
	}   
	public Date getCommentDate() {
		return this.CommentDate;
	}

	public void setCommentDate(Date CommentDate) {
		this.CommentDate = CommentDate;
	}
   
}
