package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Experience
 *
 */
@Entity

public class Experience implements Serializable {

	   
	@Id
	private int idExp;
	private String Title;
	private String descreption;
	private Date date_deb;
	private Date date_fin;
	@ManyToOne
	@JoinColumn(name="id_cv")
    private CV cvs;
	private String CompanyName;
  
	public CV getCvs() {
		return cvs;
	}
	public void setCvs(CV cvs) {
		this.cvs = cvs;
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	private static final long serialVersionUID = 1L;

	public Experience() {
		super();
	}   
	public int getIdExp() {
		return this.idExp;
	}

	public void setIdExp(int idExp) {
		this.idExp = idExp;
	}   
	public String getTitle() {
		return this.Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}   
	public String getDescreption() {
		return this.descreption;
	}

	public void setDescreption(String descreption) {
		this.descreption = descreption;
	}   
	public Date getDate_deb() {
		return this.date_deb;
	}

	public void setDate_deb(Date date_deb) {
		this.date_deb = date_deb;
	}   
	public Date getDate_fin() {
		return this.date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
   
}
