package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: media
 *
 */
@Entity

public class media implements Serializable {

	   
	@Id
	private int IdMedia;
	private String url;
	private String type;
	private Date date_media;
	@ManyToOne
	@JoinColumn(name="id_post")
    private Post posts;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_user")
    private User users;
  

	public User getUsers() {
		return users;
	}
	public void setUsers(User users) {
		this.users = users;
	}

	private static final long serialVersionUID = 1L;

	public media() {
		super();
	}   
	public int getIdMedia() {
		return this.IdMedia;
	}

	public void setIdMedia(int IdMedia) {
		this.IdMedia = IdMedia;
	}   
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public Date getDate_media() {
		return this.date_media;
	}

	public void setDate_media(Date date_media) {
		this.date_media = date_media;
	}
   
}
