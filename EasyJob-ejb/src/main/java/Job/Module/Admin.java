package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity

public class Admin   implements Serializable {

	@Id
	@GeneratedValue
	private int id_Admin;
	public int getId_Admin() {
		return id_Admin;
	}

	public void setId_Admin(int id_Admin) {
		this.id_Admin = id_Admin;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL,mappedBy="Admins")
	private List<User> users;
	private String FirstName;
	private String LastName;
	private Date DateLastLog;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}   
   
	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}   
	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}   
	public Date getDateLastLog() {
		return this.DateLastLog;
	}

	public void setDateLastLog(Date DateLastLog) {
		this.DateLastLog = DateLastLog;
	}
   
}
