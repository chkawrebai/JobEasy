package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

 
import Job.Module.Offre;

@Stateless
public class OffreEntityManager {
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	
	public void addDomain(Offre u){
		em.persist(u);
	}
	public Offre FindOffreById(int id){
		Offre req= em.find(Offre.class,id);
		return req;
		
	}
	public List<Offre> getAllOffre(){
		Query req=em.createQuery("select p from Offre p");
		return req.getResultList();
	}
	public void editOffre(Offre entity){
		em.merge(entity);
	}
	 public void removeOffre(Offre entity) {
	        em.remove(em.merge(entity));
	 }
	
	

}
