package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Comment;

@Stateless
public class CommentEntityManager {

	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	

	public Comment FindCommentByID(int id)
	{
		Comment req=em.find(Comment.class, id);
		return req;
		
	}
	
	public List<Comment> FindCommentByPost(int id)
	{
		Query req=em.createQuery("select p from Comment p where p.id_post = :id").setParameter("id", id);
		return req.getResultList();
		
	}
	public void addComment(Comment c) {
		em.persist(c);
	}
	
	public List<Comment> getAllComments() {
		Query req=em.createQuery("select p from Comment p");
		return req.getResultList();
	}
	
}
