package Services;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Admin;
import Job.Module.User;
import Job.Module.media;


 

@Stateless
public class MediaEntityManger {

	
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	
	
	public media FindMediaBYID(int id)
	{
		media req=em.find(media.class, id);
		return req;
		
	}
	public void addMedia(media u) {
		em.persist(u);
	}
	public List<media> getAllmedia() {
		Query req=em.createQuery("select p from media p");
		return req.getResultList();
	}
	
}
