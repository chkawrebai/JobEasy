package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Domains;

@Stateless
public class DomainEntityManager {

	
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	
	public void addDomain(Domains u){
		em.persist(u);
	}
	public Domains FindDomainsById(int id){
		Domains req= em.find(Domains.class,id);
		return req;
		
	}
	public List<Domains> getAllDomains(){
		Query req=em.createQuery("select p from Domains p");
		return req.getResultList();
	}
	public void editDomain(Domains entity){
		em.merge(entity);
	}
	 public void removeDomains(Domains entity) {
	        em.remove(em.merge(entity));
	 }
	
}
