package Services;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Degree;
import Job.Module.Post;


@Stateless
public class PostEntityManager {

	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	

	public Post FindPostByID(int id)
	{
		Post req=em.find(Post.class, id);
		return req;
		
	}
	public void addPost(Post p) {
		em.persist(p);
	}
	
	public List<Post> getAllPosts() {
		Query req=em.createQuery("select p from Post p");
		return req.getResultList();
	}
	 public void removePost(Post entity) {
	        em.remove(em.merge(entity));
	 }
	
}
