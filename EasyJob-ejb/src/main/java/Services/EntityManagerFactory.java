package Services;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.User;
@Stateless(name="Users")
public class EntityManagerFactory {
 
	   
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	public void addUser(User u) {
		em.persist(u);
	}
	public User FindUserById(int Id)
	{
		User req=em.find(User.class, Id);
		return req;
		
	}
	public List<User> FindUserBYType(String TypeUser)
	 {
		Query req=em.createQuery("select p from User p where p.DTYPE like :x");
		req.setParameter("x", TypeUser);
		return req.getResultList();
	}
	public List<User> getAllUser() {
		Query req=em.createQuery("select p from User p");
		return req.getResultList();
	}
	
}
