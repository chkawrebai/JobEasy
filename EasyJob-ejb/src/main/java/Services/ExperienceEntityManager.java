package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import Job.Module.Experience;

@Stateless
public class ExperienceEntityManager {
	
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
public void AddExperience(Experience e) {
		
		em.persist(e);
	}

public Experience FindExperienceById(int Id)

{
	
	Experience req=em.find(Experience.class, Id);
	return req;
	
}

public void editExperience(Experience e)
{
	
	em.merge(e);
	
}

public void removeExperience(Experience e)
{
	
	em.remove(em.merge(e));
	
}	
	
public List<Experience> getAllExperience() {
	Query req=em.createQuery("select p from Experience p");
	return req.getResultList();
}	
	

}
