package Services;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Degree;
import Job.Module.Domains;
@Stateless(name="DegreeEntityManager")
public class DegreeEntityManager {
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	
	public void addDegree(Degree u){
		
		em.persist(u);
		}
	public Degree FindDegreeById(int id){
		Degree req= em.find(Degree.class,id);
		return req;
		
	}
	public List<Degree> getAllDegree(){
		
		Query req=em.createQuery("select p from Degree p");
		return req.getResultList();
	}
	 public void removeDegree(Degree entity) {
	        em.remove(em.merge(entity));
	 }
}
