package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.JobProvider;
import Job.Module.User;
@Stateless
public class JobProviderEntityManager {

	
	
	
	   
		@PersistenceContext(unitName="EasyJob-ejb")
		private EntityManager em;
		
		public void addUser(JobProvider u) {
			em.persist(u);
		}
		public JobProvider FindUserById(int Id)
		{
			JobProvider req=em.find(JobProvider.class, Id);
			return req;
			
		}
		public void editJobprovider(JobProvider u){
			em.merge(u);
			
		}
		public void removeJobprovider(JobProvider u){
			em.remove(em.merge(u));
			
		}
		public List<JobProvider> maxidJobProvider(){
			
		List<JobProvider> id=	em.createQuery("select e.id_JobProvider from JobProvider e order by e.id_JobProvider desc ").setMaxResults(1).getResultList();
		return id;
		}
		public List<User> FindUserBYType(String TypeUser)
		 {
			Query req=em.createQuery("select p from User p where p.DTYPE like :x");
			req.setParameter("x", TypeUser);
			return req.getResultList();
		}
		public List<JobProvider> getAllUser() {
			Query req=em.createQuery("select p from User p");
			return req.getResultList();
		}
		
}
