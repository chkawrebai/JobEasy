package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.JobProvider;
import Job.Module.JobSeeker;
import Job.Module.User;

@Stateless
public class JobSeekerEntityManger {

	
	   
			@PersistenceContext(unitName="EasyJob-ejb")
			private EntityManager em;
			
			public void addJobSeeker(JobSeeker u) {
				em.persist(u);
			}
			public JobSeeker FindJobSeekerById(int Id)
			{
				JobSeeker req=em.find(JobSeeker.class, Id);
				return req;
				
			}
			public void editJobSeeker(JobSeeker u){
				em.merge(u);
				
			}
			public void removeJobSeeker(JobSeeker u){
				em.remove(em.merge(u));
				
			}
			public List<JobSeeker> maxidJobSeeker(){
				
			List<JobSeeker> id=	em.createQuery("select e.id_JobProvider from JobProvider e order by e.id_JobProvider desc ").setMaxResults(1).getResultList();
			return id;
			}
			 
			public List<JobSeeker> getAllUser() {
				Query req=em.createQuery("select p from JobSeeker p");
				return req.getResultList();
			}
}
