package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.CV;
@Stateless
public class CvEntityManager {
	
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
public void AddCv(CV c) {
		
		
		em.persist(c);
	}

public CV FindCvById(int Id)
{
	CV req=em.find(CV.class, Id);
	return req;
	
}
public void editCv(CV u){
	em.merge(u);
	
}

public void removeCv(CV u){
	em.remove(em.merge(u));
	
}

public List<CV> getAllCv() {
	Query req=em.createQuery("select p from CV p");
	return req.getResultList();
}



}
