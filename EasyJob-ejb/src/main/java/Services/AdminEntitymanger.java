package Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Job.Module.Admin;


@Stateless
public class AdminEntitymanger {
	   
	@PersistenceContext(unitName="EasyJob-ejb")
	private EntityManager em;
	
	public void addAdmin(Admin u) {
		em.persist(u);
	}
	public Admin FindAdminById(int Id)
	{
		Admin req=em.find(Admin.class, Id);
		return req;
		
	}
	public List<Admin> FindUserBYType(String TypeUser)
	 {
		Query req=em.createQuery("select p from User p where p.DTYPE like :x");
		req.setParameter("x", TypeUser);
		return req.getResultList();
	}
	public List<Admin> getAllAdmin() {
		Query req=em.createQuery("select p from User p");
		return req.getResultList();
	}
}
