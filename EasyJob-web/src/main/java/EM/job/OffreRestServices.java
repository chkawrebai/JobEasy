package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Offre;
import Services.OffreEntityManager;

@Path("OffreRestServices")
@Stateless
public class OffreRestServices {

	
	
	

	@EJB
	private OffreEntityManager em;
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	
	@Path("ListOffre")
	public Response ListOffre() {
		String response = null;	
			List<Offre> list = em.getAllOffre();
			
			response = toJSONString(list);	
			return Response.ok(response).build();
	}
	
	@POST
	@Path("createOffre")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public void createOffre(Offre u) {
        em.addDomain(u);
    }
	@PUT
    @Path("UpdateDomains/{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Offre entity) {
		em.editOffre(entity); 
         
    }
	 @DELETE
     @Path("DeleteOffre/{id}")
     public void remove(@PathParam("id") Integer id) {
	        em.removeOffre(em.FindOffreById(id));
	    }
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
