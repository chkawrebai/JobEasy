package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Domains;
import Job.Module.User;
import Job.Module.specialty;
import Services.DomainEntityManager;

@Path("DomainsRestServices")
@Stateless
public class DomainsRestServices {

	
	@EJB
	private DomainEntityManager em;
	
	
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	
	@Path("ListDomains")
	public Response ListDomains() {
		String response = null;	
			List<Domains> list = em.getAllDomains();
			
			response = toJSONString(list);	
			return Response.ok(response).build();
	}
	
	@POST
	@Path("createDomains")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public void createDomains(Domains u) {
        em.addDomain(u);
    }
	@PUT
    @Path("UpdateDomains/{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Domains entity) {
		em.editDomain(entity);
         
    }
	 @DELETE
     @Path("DeleteDomains/{id}")
     public void remove(@PathParam("id") Integer id) {
	        em.removeDomains(em.FindDomainsById(id));
	    }
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
