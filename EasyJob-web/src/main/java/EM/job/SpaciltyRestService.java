package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Post;
import Job.Module.specialty;
import Services.SpecialltyEntityManager;

@Path("SpaciltyRestService")
@Stateless
public class SpaciltyRestService {

	
	
	@EJB
	private SpecialltyEntityManager em;
	
	
	
	@GET
	@Produces("text/html")
	@Path("status")
	public Response getStatus() {

		return Response.ok("<h1>REST service is up !!!</h1>").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("ListSepecality")
	public Response ListSepecality() {
		String response = null;	
			List<specialty> list = em.getAllSpality();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	
	@Path("ListSepecalityxml")
	public List<specialty> ListSepecalityxml() {
			
			List<specialty> list = em.getAllSpality();			
			
		return list;
	}
	@GET
	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("specialtyID")
	public Response Userx(@QueryParam("id") int id) {
		String response = null;	
			specialty list = em.FindSpecialltyBYID(id);			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Path("Ajouterspecialty")
	public void Ajouterspecialty(specialty u) {
      System.out.println(u.toString());
      
     
		em.addSpacialty(u);	
		 
	}
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removespecialty/{id}")
	public void removespecialty(@PathParam("id") int id ,specialty e) {
     
		em.removespecialty((specialty) em.FindSpecialltyBYID(id));
		 
	}
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
