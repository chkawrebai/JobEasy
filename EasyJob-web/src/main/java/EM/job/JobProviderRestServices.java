package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.JobProvider;
import Job.Module.User;
import Services.EntityManagerFactory;
import Services.JobProviderEntityManager;
import Services.MediaEntityManger;

@Path("JobProviderRestServices")
@Stateless
public class JobProviderRestServices {
	 
		@EJB
		private JobProviderEntityManager em;
	
		
	

		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("listJobProvider")
		public Response listJobProvider() {
			String response = null;	
				List<JobProvider> list = em.getAllUser();			
				response = toJSONString(list);	
			return Response.ok(response).build();
		}
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("idMaxProvider")
		public Response idMaxProvider() {
			String response = null;	
			List<JobProvider>	id = em.maxidJobProvider();			
				response = toJSONString(id);	
			return Response.ok(response).build();
		}
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("Userx")
		public Response Userx(@QueryParam("id") int id) {
			String response = null;	
			JobProvider list = em.FindUserById(id);			
				response = toJSONString(list);	
			return Response.ok(response).build();
		}
		
		@POST
		@Produces(MediaType.APPLICATION_JSON)
		@Path("AjouterJobProvider")
		public void AjouterJobProvider(JobProvider u) {
	      System.out.println(u.toString());
	      
	     
			em.addUser(u);
		}
		
		
		@PUT
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/UPdate/{id}")
		public void editJobProvider(@PathParam("id") int id ,JobProvider u) {
	     
	      
	     
			em.editJobprovider(u);	
			 
		}
		
		@DELETE
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/removeJobProvider/{id}")
		public void removeJobProvider(@PathParam("id") int id ,JobProvider u) {
	     
	      
	     
			em.removeJobprovider((JobProvider) em.FindUserById(id));	
			 
		}
		public String toJSONString(Object list) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Gson gson = gsonBuilder.create();
			return gson.toJson(list);
		}
}
