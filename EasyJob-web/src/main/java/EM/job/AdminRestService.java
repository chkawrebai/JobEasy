package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import Job.Module.Admin;
import Job.Module.User;
import Services.AdminEntitymanger;

@Path("AdminRestService")
@Stateless
public class AdminRestService {

	@EJB
	private AdminEntitymanger em;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("Admins")
	public Response listAdmins() {
		String response = null;	
			List<Admin> list = em.getAllAdmin();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterAdmin")
	public void AjouterAdmin(Admin u) {
	    
		em.addAdmin(u);	
		 
	}
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
