package EM.job;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 
import Job.Module.User;
import Job.Module.media;
import Services.EntityManagerFactory;
import Services.MediaEntityManger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 *
 * @author chkaw
 */
@Path("userRESTSERVICE")
@Stateless
public class UserRESTSERVICE   {
 
	@EJB
	private EntityManagerFactory em;
	private MediaEntityManger mmmm ;
	// www.mysite.com/rest/bookservice/status
	@GET
	@Produces("text/html")
	@Path("status")
	public Response getStatus() {

		return Response.ok("<h1>REST service is up !!!</h1>").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("list")
	public Response listBooks() {
		String response = null;	
			List<User> list = em.getAllUser();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("Userx")
	public Response Userx(@QueryParam("id") int id) {
		String response = null;	
			User list = em.FindUserById(id);			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterUser")
	public void AjouterUser(User u) {
      System.out.println(u.toString());
      
     
		em.addUser(u);	
		 
	}
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
