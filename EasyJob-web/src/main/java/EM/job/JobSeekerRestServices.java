package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.JobSeeker;
import Services.JobSeekerEntityManger;;

@Path("JobSeekerRestServices")
@Stateless
public class JobSeekerRestServices {

	
	
	@EJB
	private JobSeekerEntityManger em;
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	
	@Path("ListDomains")
	public Response ListJobSeeker() {
		String response = null;	
			List<JobSeeker> list = em.getAllUser();
			
			response = toJSONString(list);	
			return Response.ok(response).build();
	}
	
	@POST
	@Path("createDomains")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public void createJobSeeker(JobSeeker u) {
        em.addJobSeeker(u);
    }
	@PUT
    @Path("UpdateJobSeeker/{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    public void editJobSeeker(@PathParam("id") Integer id, JobSeeker entity) {
		em.editJobSeeker (entity);
         
    }
	 @DELETE
     @Path("DeleteJobSeeker/{id}")
     public void remove(@PathParam("id") Integer id) {
	        em.removeJobSeeker(em.FindJobSeekerById(id));
	    }
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
