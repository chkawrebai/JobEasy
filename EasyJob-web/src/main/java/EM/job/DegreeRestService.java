package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.CV;
import Job.Module.Degree;
import Job.Module.User;
import Job.Module.specialty;
import Services.DegreeEntityManager;

@Path("DegreeRestService")
@Stateless
public class DegreeRestService {

	@EJB
	private DegreeEntityManager em;
	
	
	@GET
	@Produces( MediaType.APPLICATION_JSON)	
	@Path("ListDegree")
	public Response ListDegree() {
		String response = null;	
		List<Degree> list = em.getAllDegree();		
		response = toJSONString(list);	
	return Response.ok(response).build();
	}
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removeDegree/{id}")
	public void removeCv(@PathParam("id") int id ,CV c) {
     
      
     
		em.removeDegree ((Degree) em.FindDegreeById(id));
		 
	}
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
