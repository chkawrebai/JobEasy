package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Admin;
import Job.Module.User;
import Job.Module.media;
import Services.EntityManagerFactory;
import Services.MediaEntityManger;

@Path("MediaRESTSERVICE")
@Stateless
public class MediaRestServices {
	
	@EJB
	private MediaEntityManger em;
	private EntityManagerFactory mediiiii;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("ListMedia")
	public Response ListMedia() {
		String response = null;	
			List<media> list = em.getAllmedia();	
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterMedia")
	public void Ajoutermedia(media u) {
/*User uss =new User();
uss=mediiiii.FindUserById(2);
u.setUsers(uss);*/
		em.addMedia(u);
		 
	}
	
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
