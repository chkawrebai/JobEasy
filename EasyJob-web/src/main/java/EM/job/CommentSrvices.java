package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.CV;
import Job.Module.Comment;
import Services.CommentEntityManager;

@Path("CommentServices")
@Stateless
public class CommentSrvices {


	@EJB
	private CommentEntityManager em;
	
	
	
	@GET
	@Produces("text/html")
	@Path("status")
	public Response getStatus() {

		return Response.ok("<h1>REST service is up !!!</h1>").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("ListComments")
	public Response ListComments() {
		String response = null;	
			List<Comment> list = em.getAllComments();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	
	@Path("ListCommentsxml")
	public List<Comment> ListCommentsxml() {
			
			List<Comment> list = em.getAllComments();			
			
		return list;
	}
 
	@GET
	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("Commentx")
	public Response Commentx(@QueryParam("id") int id) {
		String response = null;	
			Comment p = em.FindCommentByID(id);			
			response = toJSONString(p);	
		return Response.ok(response).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("CommentPostx")
	public Response ListCommentsPostx(@QueryParam("id") int id) {
		String response = null;	
		List<Comment> list = em.FindCommentByPost(id);			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("AddComment")
	public void AddComment(Comment p) {

		em.addComment(p);	
		 
	}
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
