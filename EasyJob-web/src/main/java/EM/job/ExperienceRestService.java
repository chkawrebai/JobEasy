package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Experience;
import Services.ExperienceEntityManager;

@Path("ExperienceRestService")
@Stateless

public class ExperienceRestService {
	
	@EJB
	private ExperienceEntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("listExperience")
	public Response listExperience() {
		String response = null;	
			List<Experience> list = em.getAllExperience();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterExperience")
	public void AjouterExperience(Experience e) {
      
      
		em.AddExperience(e);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UPdate/{id}")
	public void editExperience(@PathParam("id") int id ,Experience e) {

		em.editExperience(e);		 
	}
	
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removeExperience/{id}")
	public void removeExperience(@PathParam("id") int id ,Experience e) {
     
		em.removeExperience((Experience) em.FindExperienceById(id));
		 
	}
	
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
