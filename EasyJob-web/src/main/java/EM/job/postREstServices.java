package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.Experience;
import Job.Module.Post;
import Services.ExperienceEntityManager;
import Services.PostEntityManager;

@Path("postREstServices")
@Stateless
public class postREstServices {

	
	
	@EJB
	private PostEntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("listPost")
	public Response listPost() {
		String response = null;	
			List<Post> list = em.getAllPosts();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterPost")
	public void AjouterExperience(Post e) {
      
      
		em.addPost(e);
	}
	
 
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removePost/{id}")
	public void removeExperience(@PathParam("id") int id ,Post e) {
     
		em.removePost((Post) em.FindPostByID(id));
		 
	}
	
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
