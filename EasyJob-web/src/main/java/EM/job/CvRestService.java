package EM.job;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Job.Module.CV;

import Services.CvEntityManager;


@Path("CvRestService")
@Stateless
public class CvRestService {
	
	@EJB
	private CvEntityManager em;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("listCv")
	public Response listCv() {
		String response = null;	
			List<CV> list = em.getAllCv();			
			response = toJSONString(list);	
		return Response.ok(response).build();
	}
	

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("AjouterCv")
	public void AjouterCv(CV c) {
      
      
		em.AddCv(c);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/UPdate/{id}")
	public void editCv(@PathParam("id") int id ,CV c) {

		em.editCv(c);		 
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removeCv/{id}")
	public void removeCv(@PathParam("id") int id ,CV c) {
     
      
     
		em.removeCv((CV) em.FindCvById(id));
		 
	}
	

	
	public String toJSONString(Object list) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		return gson.toJson(list);
	}
}
